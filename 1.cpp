#include <iostream>

using namespace std;

/*
 * Функция вывода массива
 */

void outArray(int* array)
{
    cout << "|";
    for (int i = 0; i < sizeof(array); i++){
        cout << array[i] << "|";
    }
    cout << "\n";
}

/*
 * Функция нахождения минимального элемента массива
 */

int minElement(int *array)
{
    int min = array[0];

    for (int i = 1; i < sizeof(array); i++)
    {
        if (array[i] < min)
        {
            min = array[i];
        }
    }

    return min;
}

/*
 * Функция нахождения суммы между первым и последним
 * положительным элементами массива.
 */

int sumPositive(int* array)
{
    int begin, end, o = 0;
    int sum = 0;

    // Находим первый положительный и последний положительный элемент
    for (int i = 0; i < sizeof(array); i++)
    {
        if (array[i] > 0)
        {
            if (o == 0){
                begin = i + 1;
                o++;
            }
            else{
                end = i;
            }
        }
    }

    for (int s = begin; s < end; s++)
    {
        sum += array[s];
    }

    return sum;
}

/*
 * Функция, которая располагает сначала нули массива, а потом всё остальное
 */

void zeroBegin(int* array)
{
    int i = 0;
    int j = 0;
    int n = sizeof(array);
    for(i=0;i<n;i++)
    {
        if (array[i]==0) {
            for (j = 0; (j<=i)&&(array[j]==0); j++);
            array[i]=array[j];
            array[j]=0;
        }
    }
}

int main(int argc, char const *argv[])
{
    int array[] = {-1, 3, 2, 5, 0, 0, 2, -2};

    outArray(array);

    printf("Сумма между первым и последним положительным элементом: %d\n" , sumPositive(array));
    printf("Минимальный элемент массива: %d\n" , minElement(array));
    printf("Сортированный массив: ");

    zeroBegin(array);

    outArray(array);

    return 0;
}